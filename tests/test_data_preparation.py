import unittest
from .context import get_data_resource_path, DataPreparation
import pprint


class TestDataPreparation(unittest.TestCase):
    column_name = "overall_survival"

    @classmethod
    def setUpClass(cls) -> None:
        filepath = get_data_resource_path("METABRIC_RNA_Mutation.csv")
        filepathout = get_data_resource_path("processed_rna_mutation.csv")
        cls.dp = DataPreparation(filepath=filepath, filepathout=filepathout)

    def test_fill_categorical_missing_values(self):
        self.dp.fill_categorical_missing_values()
        self.assertEqual(self.dp.get_count_of_missing_values(), 0)
        self.assertEqual(self.dp.get_ratio_of_missing_values(), 0)
        self.assertEqual(self.dp.get_count_of_rows_with_missing_values(), 0)
        self.assertEqual(self.dp.get_count_of_columns_with_missing_values(), 0)
        self.assertEqual(self.dp.get_ratio_of_rows_with_missing_values(), 0)
        self.assertEqual(self.dp.get_ratio_of_columns_with_missing_values(), 0)

    def test_get_column_list_with_unique_value(self):
        self.assertEqual(self.dp.get_column_list_with_unique_value(), [])

    def test_get_high_correlated_columns_dict(self):
        # self.dp.get_correlation_to_predict_column(self.column_name)
        my_dict = self.dp.get_high_correlated_columns_dict()
        pprint.pprint(my_dict)

    def test_clean_df(self):
        dtypes = self.dp.df.dtypes
        self.dp.clean_df()

