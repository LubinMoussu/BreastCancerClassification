import unittest
from .context import get_data_resource_path, BreastCancerClassification


class TestBreastCancerClassification(unittest.TestCase):
    column_to_remove = ["death_from_cancer", "overall_survival_months", "cancer_type", "patient_id", "cohort"]
    column_name = "overall_survival"

    @classmethod
    def setUpClass(cls) -> None:
        filepath = get_data_resource_path("processed_rna_mutation.csv")
        cls.bcc = BreastCancerClassification(filepath)

    def test_XGBClassifier(self):
        # Predict breast cancer survival
        # self.bcc.explore_categorical_data()
        self.bcc.prepare_df()
        ac = self.bcc.setup_classification_workflow(column_name=self.column_name)
        print(ac)
        self.bcc.get_feature_importances()

    def test_XGBoostRregressor(self):
        self.column_to_remove = ["death_from_cancer", "cancer_type", "patient_id", "cohort", "overall_survival"]
        self.column_name = "overall_survival_months"
        # self.bcc.explore_categorical_data()
        self.bcc.prepare_df()
        self.bcc.setup_regression_workflow(column_name=self.column_name)
        mae = self.bcc.get_mae()
        rmse = self.bcc.get_rmse()
        print(mae)
        print(rmse)
        self.bcc.get_feature_importances()

    def test_random_forest(self):
        self.column_to_remove = ["death_from_cancer", "cancer_type", "patient_id", "cohort", "overall_survival"]
        self.column_name = "overall_survival_months"
        # self.bcc.explore_categorical_data()
        self.bcc.prepare_df()
        self.bcc.setup_random_forest_workflow(column_name=self.column_name)

    def test_random_forest_grid_search(self):
        self.column_to_remove = ["death_from_cancer", "cancer_type", "patient_id", "cohort", "overall_survival"]
        self.column_name = "overall_survival_months"
        # self.bcc.explore_categorical_data()
        self.bcc.prepare_df()
        self.bcc.setup_random_forest_grid_search(column_name=self.column_name)

    def test_(self):
        a = {'n_estimators': 600, 'min_samples_split': 5, 'min_samples_leaf': 4, 'max_features': 'auto',
             'max_depth': 30, 'bootstrap': True}
