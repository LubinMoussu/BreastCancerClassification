import os
import sys

sys.path.insert(0, os.path.abspath(os.path.join(os.path.dirname(__file__), '..')))
from breast_cancer_classification.data_preparation import DataPreparation
from breast_cancer_classification.breast_cancer_classification import BreastCancerClassification


def get_resource_path(filename):
    return os.path.join(
        os.path.dirname(os.path.realpath(__file__)), filename,
    )


def get_tests_resource_path(filename):
    return os.path.join(
        os.path.dirname(os.path.dirname(os.path.realpath(__file__))),
        "tests",
        filename,
    )


def get_data_resource_path(filename):
    return os.path.join(
        os.path.dirname(os.path.dirname(os.path.realpath(__file__))),
        "data",
        filename,
    )
