import warnings
import numpy as np
import pandas
import pandas as pd

# download stpwords

threshold_correlation = 0.8

# nltk.download('stopwords')

warnings.filterwarnings('ignore')
import warnings
import re

# download stpwords

# import nltk for stopwords
from nltk.corpus import stopwords

from sklearn.preprocessing import LabelEncoder
# Filter warnings
import warnings

warnings.filterwarnings('ignore')

pandas.options.mode.chained_assignment = None  # default='warn'
from collections import Counter

import seaborn as sns

warnings.filterwarnings('ignore')
# Larger scale for plots in notebooks
sns.set_context('notebook')

stop_words = set(stopwords.words('english'))


class DataPreparation:
    missing_values = [np.nan]
    categorical_missing_values_handler = "replace_mfv"
    column_to_remove = ["death_from_cancer", "overall_survival_months", "cancer_type", "patient_id", "cohort"]

    def __init__(self, filepath: str, filepathout: str):
        if not isinstance(filepath, str) or not isinstance(filepathout, str):
            raise TypeError

        self.df = pd.read_csv(filepath, delimiter=",", index_col=False)
        self.filepath = filepath
        self.filepathout = filepathout

    def is_any_missing_values_across_columns(self):
        return self.df.isnull()

    def is_any_missing_values(self):
        return self.df.isnull().values.any()

    def get_count_of_missing_values(self):
        return self.df.isnull().sum().sum()

    def get_count_of_rows_with_missing_values(self):
        return self.df.shape[0] - self.df.dropna().shape[0]

    def get_count_of_columns_with_missing_values(self):
        return self.df.shape[1] - self.df.dropna(axis='columns').shape[1]

    def get_ratio_of_rows_with_missing_values(self):
        return round(self.get_count_of_rows_with_missing_values() / self.df.shape[0] * 100, 3)

    def get_ratio_of_columns_with_missing_values(self):
        return round(self.get_count_of_columns_with_missing_values() / self.df.shape[1] * 100, 3)

    def fill_missing_values_with_column_mean(self):
        self.df = self.df.fillna(self.df.mean())

    def get_non_standard_missing_values(self):
        column_with_missing_values = self.df.columns[self.df.isnull().any()].tolist()
        for column in column_with_missing_values:
            print(self.df[column].unique())

    def get_ratio_of_missing_values(self):
        return round(self.get_count_of_missing_values() / self.df.size * 100, 3)

    def fill_categorical_missing_values(self):
        """

        :return:
        """
        if self.categorical_missing_values_handler == "drop_row":
            self.df = self.df.dropna(axis=0, inplace=True)
        elif self.categorical_missing_values_handler == "drop_column":
            self.df = self.df.dropna(axis=1, inplace=True)
        elif self.categorical_missing_values_handler == "replace_mfv":
            self.df = self.df.fillna(self.df.mode().iloc[0])

    def get_stats_on_missing_values(self):
        print("get_count_of_missing_values: ", self.get_count_of_missing_values())
        print("get_ratio_of_missing_values: ", self.get_ratio_of_missing_values())
        print("get_count_of_rows_with_missing_values: ", self.get_count_of_rows_with_missing_values())
        print("get_count_of_columns_with_missing_values: ", self.get_count_of_columns_with_missing_values())
        print("get_ratio_of_rows_with_missing_values: ", self.get_ratio_of_rows_with_missing_values())
        print("get_ratio_of_columns_with_missing_values: ", self.get_ratio_of_columns_with_missing_values())

    def convert_mutation_object_to_int(self):
        """
        Replace mutation data with binary
        :return:
        """
        regex = r"_mut$"
        mutation_clmn_lst = [elt for elt in self.df.columns if re.search(regex, elt)]
        for column in mutation_clmn_lst:
            self.df[column][self.df[column] != "0"] = "1"
        self.df[mutation_clmn_lst] = self.df[mutation_clmn_lst].apply(pandas.to_numeric)

    def shuffle_df(self):
        """
        Shuffle rows
        :return:
        """
        self.df = self.df.sample(frac=1).reset_index(drop=True)

    def remove_columns(self, to_drop):
        if not isinstance(to_drop, list):
            raise TypeError
        self.df.drop(to_drop, axis=1, inplace=True)

    def remove_rows(self, to_drop):
        if not isinstance(to_drop, list):
            raise TypeError
        self.df.drop(to_drop, axis=0, inplace=True)

    def get_column_list_with_unique_value(self):
        """
        Get column list of column containing only one variant
        :return:
        """
        return [column for column in self.df.columns.tolist() if len(self.df[column].unique()) <= 1]

    def get_column_occurrences(self, column_name: str):
        column_values = self.df[column_name]
        return Counter(column_values)

    def get_df_column_occurrences(self):
        column_list = self.df.columns.tolist()
        return {elt: self.get_column_occurrences(column_name=elt) for elt in column_list}

    def object_dtype_column_list(self):
        return [column for column in self.df.columns.tolist() if self.df[column].dtype == "object"]

    def normalize_textual_data(self, string: str):
        """

        :param string:
        :return:
        """
        if string not in self.missing_values:
            # convert to lower case and remove white spaces
            lower_string = str(string).lower().strip()
            # remove all punctuation except words and space
            no_punc_string = re.sub(r'[^\w\s]', '', lower_string)
            word_list = no_punc_string.split(" ")
            no_stpwords_string = " ".join([elt for elt in word_list if elt not in stop_words])
            return no_stpwords_string
        return string

    def is_column_containing_value_inconsistency(self, column: str) -> bool:
        """
        Compare unique value column with processed unique values to see issues in column values (lower, upper cases, whitespace)
        :param column:
        :return:
        """
        unique_values = [elt for elt in self.df[column].unique() if elt not in self.missing_values]
        lower_values = list(set([self.normalize_textual_data(elt) for elt in unique_values]))
        return len(unique_values) != len(lower_values)

    def clean_categorical_column(self):
        """
        Map
        :return:
        """
        object_column = self.object_dtype_column_list()  # get categorical columns
        self.df.loc[:, object_column] = self.df.loc[:, object_column].applymap(
            self.normalize_textual_data)  # apply normalize_textual_data to all categorical values

    def clean_col_names(self):
        """
        Simple function to convert the column
        names of a dataframe to snake_case and lower case.
        """
        # Get all the col names as lower and snake_case in a list
        new_col_names = [
            column.strip().replace(' ', '_').lower().rstrip("\n") for column in self.df.columns
        ]
        # Rename the column names
        self.df.columns = new_col_names

    def find_duplicates(self) -> bool:
        duplicates = self.df.duplicated()
        return bool(self.df[duplicates].shape[0])

    def clean_specific_columns(self):
        self.clean_categorical_column()

    def drop_full_duplicates(self):
        self.df.drop_duplicates(inplace=True)

    @staticmethod
    def get_frequency_distribution_categories(df):
        """
        Get column value frequency and count of distinct categories
        :param df:
        :return:
        """
        for column in df.columns:
            print("-" * 100)
            print(df[column].value_counts())
            # print(df[column].value_counts().count())

    def explore_categorical_data(self):
        cat_df = self.df.select_dtypes(include=['object']).copy()
        print(cat_df.isnull().sum())
        self.get_frequency_distribution_categories(cat_df)

    def delete_rows_with_outliers(self):
        """
        Remove rows given matching conditions
        :return:
        """
        # Remove this row because it is an outlier on 3 categorical feature: cancer_type_detailed, oncotree_code, tumor_other_histologic_subtype
        self.df.drop(self.df[self.df.cancer_type_detailed == "Metaplastic Breast Cancer"].index, inplace=True)

    def get_high_correlation_column(self, corrmat: pandas.core.frame.DataFrame, column_name: str):
        """

        :param corrmat:
        :param column_name:
        :return:
        """
        os_corrmat = corrmat[column_name]
        rows_sup = os_corrmat[os_corrmat > threshold_correlation].dropna()  # positive high correlation
        rows_inf = os_corrmat[os_corrmat < -threshold_correlation].dropna()  # negative high correlation
        result = pd.concat([rows_sup, rows_inf])
        result.drop(column_name, inplace=True)
        return result

    def get_correlation_to_predict_column(self, column_name: str):
        """
        Get column rows that have high correlation with column to predict
        :param column_name:
        :return:
        """
        if not isinstance(column_name, str):
            raise TypeError

        corrmat = self.df.corr()
        correlation = self.get_high_correlation_column(corrmat, column_name)
        return list(correlation.index)

    def get_high_correlated_columns(self):
        """

        :return:
        """
        corrmat = self.df.corr()
        for column_name in corrmat.columns:
            correlation = self.get_high_correlation_column(corrmat, column_name)
            yield column_name, list(correlation.index)

    def get_high_correlated_columns_dict(self):
        """

        :return:
        """
        high_correlated_column_list = self.get_high_correlated_columns()
        return {k: v for k, v in dict(high_correlated_column_list).items() if v}

    def clean_df(self):
        self.drop_full_duplicates()  # drop duplicated rows
        self.df.interpolate(inplace=True)  # Fill NaN values
        self.remove_columns(self.column_to_remove)  # Drop columns too correlated or irrelevant
        self.delete_rows_with_outliers()  # Drop rows containing categorical outliers

        self.clean_categorical_column()  # Apply normalization to text values
        self.convert_mutation_object_to_int()
        self.fill_categorical_missing_values()
        if not self.is_any_missing_values():  # Make sure there is no missing value
            self.clean_col_names()
            self.shuffle_df()
            self.df.to_csv(self.filepathout, sep='\t', encoding='utf-8', index=False)


