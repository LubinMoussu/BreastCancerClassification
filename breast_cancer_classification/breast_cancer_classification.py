import warnings

import numpy as np
import pandas
import pandas as pd
from sklearn.ensemble import RandomForestRegressor
from sklearn.inspection import permutation_importance
from sklearn.model_selection import RandomizedSearchCV

# download stpwords
import nltk

nltk.download('stopwords')

# import nltk for stopwords
from nltk.corpus import stopwords

warnings.filterwarnings('ignore')
from sklearn.preprocessing import MinMaxScaler, LabelEncoder, OneHotEncoder, StandardScaler, RobustScaler
import xgboost as xgb
from sklearn.model_selection import train_test_split
from sklearn.metrics import accuracy_score, mean_squared_error, mean_absolute_error
import warnings
import re

warnings.filterwarnings('ignore')

pandas.options.mode.chained_assignment = None  # default='warn'
from collections import Counter


def f(rng): return rng.random(1)


MIN_SAMPLES_LEAF = 4
MIN_SAMPLES_SPLIT = 5
MAX_FEATURES = "auto"
SCORING = 'r2'
N_JOBS = None
VERBOSE = 2
CV = 3
N_ITER = 100
MAX_DEPTH = 30
RANDOM_STATE = 42
N_ESTIMATORS = 600
BOOtSTRAP = True


class BreastCancerClassification:
    missing_values = [np.nan]
    categorical_missing_values_handler = "replace_mfv"

    def __init__(self, filepath: str):
        if not isinstance(filepath, str):
            raise TypeError

        self.df = pandas.read_csv(filepath, delimiter=",")
        self.features = None
        self.labels = None
        self.x = None
        self.y = None
        self.data_dmatrix = None
        self.x_train = None
        self.x_test = None
        self.y_train = None
        self.y_test = None
        self.model = None
        self.scaler = None
        self.y_pred = None
        self.categorical_encoder = LabelEncoder()

    def label_encoding(self, column):
        self.df[column] = self.categorical_encoder.fit_transform(self.df[column])

    def one_hot_encoding(self, column):
        self.df = pd.get_dummies(self.df, columns=[column], prefix=[column])

    def get_object_dtype_column_list(self):
        return self.df.select_dtypes(include=[np.object])

    def encode_categorical_date(self):
        object_column = self.get_object_dtype_column_list()  # get categorical columns
        self.df[object_column] = self.df[object_column].astype('category')  # convert object to category dtype
        for column in object_column:
            # self.label_encoding(column)  # convert value to a number
            self.one_hot_encoding(column)

    def shuffle_df(self):
        """
        Shuffle rows
        :return:
        """
        self.df = self.df.sample(frac=1).reset_index(drop=True)

    def prepare_df(self):
        self.categorical_encoder = OneHotEncoder()
        self.encode_categorical_date()
        self.shuffle_df()

    def initialize_scaler(self, scaler: str):
        """

        :param scaler:
        :return:
        """
        if not isinstance(scaler, str):
            raise TypeError
        if scaler == "MinMaxScaler":
            self.scaler = MinMaxScaler((-1, 1))
        if scaler == "StandardScaler":
            self.scaler = StandardScaler()
        if scaler == "RobustScaler":
            self.scaler = RobustScaler()

    def get_features_labels(self):
        """
        get data from self.x and self.y
        :return:
        """
        self.features = self.x.values[:, 1:]
        self.labels = self.y.values

    def onehot_encode_df(self):
        self.df = pd.get_dummies(self.df)

    def split_df_into_x_and_y(self, column_name: str):
        """
        split df into x and y set given the column_name
        :param column_name:
        :return:
        """
        if not isinstance(column_name, str):
            raise TypeError
        self.x = self.df.loc[:, self.df.columns != column_name]
        self.y = self.df.loc[:, column_name]

    def split_training_testing_sets(self, test_size=0.2, random_state=7):
        if not isinstance(test_size, float) or not isinstance(random_state, int):
            raise TypeError
        self.x_train, self.x_test, self.y_train, self.y_test = train_test_split(
            self.scaler.fit_transform(self.features),
            self.labels, test_size=test_size,
            random_state=random_state)

    def initiate_classifier(self, classifier: str):
        if not isinstance(classifier, str):
            raise TypeError
        if classifier == "XGBClassifier":
            self.model = xgb.XGBClassifier()
        if classifier == "RandomForestRegressor":
            self.model = RandomForestRegressor(n_estimators=N_ESTIMATORS,
                                               min_samples_split=MIN_SAMPLES_SPLIT,
                                               min_samples_leaf=MIN_SAMPLES_LEAF,
                                               max_features=MAX_FEATURES,
                                               max_depth=MAX_DEPTH,
                                               bootstrap=BOOtSTRAP,
                                               random_state=RANDOM_STATE)

    def fit_gradient_boosting_model(self):
        self.model.fit(self.x_train, self.y_train)

    def generate_y_pred(self):
        self.y_pred = self.model.predict(self.x_test)

    def initiate_regressor(self):
        self.model = xgb.XGBRegressor(objective='reg:linear', colsample_bytree=0.3, learning_rate=0.1,
                                      max_depth=5, alpha=10, n_estimators=10)

    def get_mae(self):
        """
        get mean_absolute_error from prediction
        :return:
        """
        return mean_absolute_error(self.y_test, self.y_pred)

    def get_rmse(self):
        """
        get mean_absolute_error from prediction
        :return:
        """
        return np.sqrt(mean_squared_error(self.y_test, self.y_pred))

    def linear_regression_feature_importance(self):
        importances = self.model.feature_importances_
        feature_list = self.df.columns
        feature_importances = [(feature, importance) for feature, importance in
                               zip(feature_list, importances)]  # Sort the feature importances by most important first
        feature_importances = sorted(feature_importances, key=lambda x: x[1],
                                     reverse=True)  # Print out the feature and importances
        for pair in feature_importances:
            print('Variable: {:20} Importance: {}'.format(*pair))

    def logistic_regression_feature_importance(self):
        importance = self.model.coef_[0]
        print(importance)

    def decision_tree_feature_importance(self):
        importances = self.model.feature_importances_
        feature_list = self.df.columns
        feature_importances = [(feature, importance) for feature, importance in
                               zip(feature_list, importances)]  # Sort the feature importances by most important first
        feature_importances = sorted(feature_importances, key=lambda x: x[1],
                                     reverse=True)  # Print out the feature and importances
        for pair in feature_importances:
            print('Variable: {:20} Importance: {}'.format(*pair))

    def permutation_based_feature_importance(self):
        perm_importance = permutation_importance(self.model, self.x_test, self.y_test)
        sorted_idx = perm_importance.importances_mean.argsort()
        print(sorted_idx)
        print(len(sorted_idx))
        print(len(self.x.columns))

    def shapley_values_feature_importance(self):
        import shap
        explainer = shap.TreeExplainer(self.model)
        shap_values = explainer.shap_values(self.x_test)

    def get_feature_importances(self):
        # self.linear_regression_feature_importance()
        self.logistic_regression_feature_importance()
        self.decision_tree_feature_importance()

    def setup_regression_workflow(self, column_name: str):
        self.split_df_into_x_and_y(column_name=column_name)
        self.get_features_labels()
        self.initialize_scaler("MinMaxScaler")
        self.split_training_testing_sets()
        self.initiate_regressor()
        self.fit_gradient_boosting_model()
        self.generate_y_pred()

    def setup_classification_workflow(self, column_name: str):
        self.split_df_into_x_and_y(column_name=column_name)
        self.get_features_labels()
        self.initialize_scaler("MinMaxScaler")
        self.split_training_testing_sets()
        self.initiate_classifier("XGBClassifier")
        self.fit_gradient_boosting_model()
        self.generate_y_pred()
        return accuracy_score(self.y_test, self.y_pred)

    def setup_random_forest_workflow(self, column_name: str):
        self.split_df_into_x_and_y(column_name=column_name)
        self.get_features_labels()
        self.initialize_scaler("MinMaxScaler")
        self.split_training_testing_sets()
        self.initiate_classifier("RandomForestRegressor")
        self.fit_gradient_boosting_model()
        self.generate_y_pred()
        errors = abs(self.y_test - self.y_pred)
        print(round(np.mean(errors), 2))
        mape = 100 * (errors / self.y_test)
        accuracy = 100 - np.mean(mape)
        print('Accuracy:', round(accuracy, 2), '%.')
        # self.decision_tree_feature_importance()
        self.permutation_based_feature_importance()

    def get_random_hyperparameter_grid(self):

        # Number of trees in random forest
        n_estimators = [int(x) for x in np.linspace(start=200, stop=2000, num=10)]
        # Number of features to consider at every split
        max_features = ['auto', 'sqrt']
        # Maximum number of levels in tree
        max_depth = [int(x) for x in np.linspace(10, 110, num=11)]
        # Minimum number of samples required to split a node
        min_samples_split = [2, 5, 10]
        # Minimum number of samples required at each leaf node
        min_samples_leaf = [1, 2, 4]
        # Method of selecting samples for training each tree
        bootstrap = [True, False]

        random_grid = {'n_estimators': n_estimators,
                       'max_features': max_features,
                       'max_depth': max_depth,
                       'min_samples_split': min_samples_split,
                       'min_samples_leaf': min_samples_leaf,
                       'bootstrap': bootstrap}

        rf_random = RandomizedSearchCV(estimator=self.model, param_distributions=random_grid, n_iter=N_ITER, cv=CV,
                                       verbose=2, random_state=42)

        rf_random.fit(self.x_train, self.y_train)
        print(rf_random.best_params_)

    def setup_random_forest_grid_search(self, column_name: str):
        self.split_df_into_x_and_y(column_name=column_name)
        self.get_features_labels()
        self.initialize_scaler("MinMaxScaler")
        self.split_training_testing_sets()
        self.initiate_classifier("RandomForestRegressor")
        self.get_random_hyperparameter_grid()
