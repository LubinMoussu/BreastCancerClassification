<h1 align="center">Breast cancer classification</h1>
<p align="center">Applied machine learning to Breast Cancer Gene Expression Profiles</p>

## Project dependencies
- pandas
- scikit-learn
- xgboost
- seaborn
## Project parts
- Data Acquisition: Breast Cancer Gene Expression Profiles (METABRIC)
- Data cleaning and exploratory
- Data Preparation : One-Hot Encoding
- Make Predictions on the Test Set: Random Forest, XGBoost
- Report variable importances
- Hyperparameter tuning: GridSearchCV and RandomizedSearchCV